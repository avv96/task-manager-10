package ru.tsc.vinokurov.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

}
