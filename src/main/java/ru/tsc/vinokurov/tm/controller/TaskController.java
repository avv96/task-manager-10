package ru.tsc.vinokurov.tm.controller;

import ru.tsc.vinokurov.tm.api.controller.ITaskController;
import ru.tsc.vinokurov.tm.api.service.ITaskService;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    final private ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> taskList = taskService.findAll();
        for (final Task task: taskList) {
            if (task == null) continue;
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

}
